
import java.util.Iterator;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author uriel
 */
public class AdministradorDeMemoria {
    Memoria memory;
    int lastIndex=0;
    private AdministradorDeMemoria() {
        memory = Memoria.getInstance();
    }
    public void primerAjuste(Object o){
        int k=0;
        boolean flag=false;
        if(o.getClass() == (Proceso.class)){
            Proceso procesoAux = (Proceso) o;
            while(k<memory.lista.size()){
                try{
                    if(memory.lista.get(k).getClass()==(Hueco.class)){
                        Hueco huecoAux = (Hueco)memory.lista.get(k);
                        if(huecoAux.size>=procesoAux.getSize()){
                            System.out.println("Entro");                            
                            flag=true;
                            if(huecoAux.size==procesoAux.getSize()){
                                memory.lista.remove(k);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex = k;
                            }else{
                                huecoAux.size-=procesoAux.getSize();
                                memory.lista.remove(k);
                                memory.lista.add(k, huecoAux);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex=k+1;
                            }
                            break;
                        }else{
                            k++;
                            System.out.println("No entro");
                        }
                    }else{
                        k++;
                    }
                }catch(Exception e){
                    break;
                }
            }
            if(!flag){
                System.out.println("The size of process is too large");
                JOptionPane.showMessageDialog(null, "No hay huecos disponibles para el proceso", "ERROR", 1);
            }
            System.out.println("Inserting process...");
        }
    }
    public void siguienteAjuste(Object o){
        int k;
        k = lastIndex;
        boolean flag=false,flag2=false;
        if(o.getClass() == (Proceso.class)){
            Proceso procesoAux = (Proceso) o;
            while(k<memory.lista.size()){
                try{
                    if(memory.lista.get(k).getClass()==(Hueco.class)){
                        Hueco huecoAux = (Hueco)memory.lista.get(k);
                        if(huecoAux.size>=procesoAux.getSize()){
                            System.out.println("Entro");                            
                            flag=true;
                            if(huecoAux.size==procesoAux.getSize()){
                                memory.lista.remove(k);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex = k;
                            }else{
                                huecoAux.size-=procesoAux.getSize();
                                memory.lista.remove(k);
                                memory.lista.add(k, huecoAux);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex=k+1;
                            }
                            flag2=true;
                            break;
                        }else{
                            k++;
                            System.out.println("No entro");
                        }
                    }else{
                        k++;
                    }
                }catch(Exception e){
                    break;
                }
            }
            if(!flag2){
                k=0;
                while(lastIndex>=k){
                try{
                    if(memory.lista.get(k).getClass()==(Hueco.class)){
                        Hueco huecoAux = (Hueco)memory.lista.get(k);
                        if(huecoAux.size>=procesoAux.getSize()){
                            System.out.println("Entro");                            
                            flag=true;
                            if(huecoAux.size==procesoAux.getSize()){
                                memory.lista.remove(k);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex = k;
                            }else{
                                huecoAux.size-=procesoAux.getSize();
                                memory.lista.remove(k);
                                memory.lista.add(k, huecoAux);
                                memory.lista.add(k, procesoAux);
                                this.lastIndex=k+1;
                            }
                            break;
                        }else{
                            k++;
                            System.out.println("No entro");
                        }
                    }else{
                        k++;
                    }
                }catch(Exception e){
                    break;
                }
            }
            }
            if(!flag){
                System.out.println("The size of process is too large");
                JOptionPane.showMessageDialog(null, "No hay huecos disponibles para el proceso", "ERROR", 1);
            }
            if(flag)System.out.println("Inserting process...");
        }
    }
    public void mejorAjuste(Object o){
        int k=0, huecoMin=Integer.MAX_VALUE, indexMin=0;
        Hueco huecoAux;
        boolean flag=false;
        if(o.getClass() == (Proceso.class)){
            Proceso procesoAux = (Proceso) o;
            while(k<memory.lista.size()){
                try{
                    if(memory.lista.get(k).getClass()==(Hueco.class)){
                        huecoAux = (Hueco)memory.lista.get(k);
                        if(huecoAux.size>=procesoAux.getSize()){
                            if(huecoAux.size<huecoMin){
                                huecoMin=huecoAux.size;
                                indexMin = k;
                                flag = true;
                                System.out.println("in");
                                k++;
                            }else{
                                k++;
                            }
                        }else{
                            k++;
                            System.out.println("No entro");
                        }
                    }else{
                        k++;
                    }
                }catch(Exception e){
                    break;
                }
            }
            System.out.println("Now, we add the process into the List");
            if(flag){
                huecoAux = (Hueco)memory.lista.get(indexMin);
                System.out.println("Inserting process...");
                if(huecoAux.size==procesoAux.getSize()){
                    memory.lista.remove(indexMin);
                    memory.lista.add(indexMin, procesoAux);
                    this.lastIndex = indexMin;
                }else{
                    huecoAux.size-=procesoAux.getSize();
                    memory.lista.remove(indexMin);
                    memory.lista.add(indexMin, huecoAux);
                    memory.lista.add(indexMin, procesoAux);
                    this.lastIndex=k+1;
                }
                System.out.println("Process inserted succesfully!");
            }else{
                JOptionPane.showMessageDialog(null, "No hay huecos disponibles para el proceso", "ERROR", 1);
                System.out.println("The size of process is too large");
            }
        }
    }
    public void peorAjuste(Object o){
        int k=0, huecoMax=0, indexMax=0;
        Hueco huecoAux;
        boolean flag=false;
        if(o.getClass() == (Proceso.class)){
            Proceso procesoAux = (Proceso) o;
            while(k<memory.lista.size()){
                try{
                    if(memory.lista.get(k).getClass()==(Hueco.class)){
                        huecoAux = (Hueco)memory.lista.get(k);
                        if(huecoAux.size>=procesoAux.getSize()){
                            if(huecoAux.size>huecoMax){
                                huecoMax=huecoAux.size;
                                indexMax = k;
                                flag = true;
                                System.out.println("in");
                                k++;
                            }else{
                                k++;
                            }
                        }else{
                            k++;
                            System.out.println("No entro");
                        }
                    }else{
                        k++;
                    }
                }catch(Exception e){
                    break;
                }
            }
            System.out.println("Now, we add the process into the List");
            
            if(flag){
                huecoAux = (Hueco)memory.lista.get(indexMax);
                System.out.println("Inserting process...");
                if(huecoAux.size==procesoAux.getSize()){
                    memory.lista.remove(indexMax);
                    memory.lista.add(indexMax, procesoAux);
                    this.lastIndex = indexMax;
                }else{
                    huecoAux.size-=procesoAux.getSize();
                    memory.lista.remove(indexMax);
                    memory.lista.add(indexMax, huecoAux);
                    memory.lista.add(indexMax, procesoAux);
                    this.lastIndex=k+1;
                }
                System.out.println("Process inserted succesfully!");
            }else{
                JOptionPane.showMessageDialog(null, "No hay huecos disponibles para el proceso", "ERROR", 1);
                System.out.println("The size of process is too large");
            }
        }
    }
    public static AdministradorDeMemoria getInstance() {
        return AdministradorDeMemoriaHolder.INSTANCE;
    }
    
    private static class AdministradorDeMemoriaHolder {

        private static final AdministradorDeMemoria INSTANCE = new AdministradorDeMemoria();
    }
}
