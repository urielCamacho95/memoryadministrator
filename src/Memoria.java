
import java.util.ArrayList;
import java.util.List;
/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author uriel
 */
public class Memoria {
    List<Object> lista;
    int size; //Size of memory address space     
    private Memoria() {
        lista = new ArrayList();
        //this.size = 64;
    }
    public void defaultMemory(){
        // create process one and store his values
        Hueco hueco1 = new Hueco(9);
        // create process one and store his values
        Proceso proc1 = new Proceso("PA",5,1);        
        // create process one and store his values
        Hueco hueco2 = new Hueco(3);
        // create process one and store his values
        Proceso proc2 = new Proceso("PB",4,2);
        // create process one and store his values
        Hueco hueco3 = new Hueco(2);
        // create process one and store his values
        Proceso proc3 = new Proceso("PC",7,3);
        // create process one and store his values
        Hueco hueco4 = new Hueco(5);
        lista.add(hueco1);
        lista.add(proc1);
        lista.add(hueco2);
        lista.add(proc2);
        lista.add(hueco3);
        lista.add(proc3);
        lista.add(hueco4);
    }
    public int getNumberOfProcess(){
        return lista.size();
    }
    public Object processName(int index){
       return lista.get(index);
    }
    
    public void printProcess(){
    }
    public static Memoria getInstance() {
        return MemoriaHolder.INSTANCE;
    }
    
    private static class MemoriaHolder {

        private static final Memoria INSTANCE = new Memoria();
    }
}
