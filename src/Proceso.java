/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author uriel
 */
public class Proceso {
    //Process have a name, a size and one id;
    String name;
    private final int size;
    private final int id;
    //Process constructor

    /**
     *
     * @param name means the name of Process
     * @param size means the size of process in memory
     * @param id means the process ID PID
     */
    public Proceso(String name,int size, int id){
        this.name = name;
        this.size = size;
        this.id = id;
    }
    public int getSize(){
        return this.size;
    }
    public int getId(){
        return this.id;
    }
    
    
}
